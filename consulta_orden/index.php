<?php
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';

  include '../DbSetup.php';
  ?>
  
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Compras</title>
  <meta charset="utf-8">
</head>
<body>
  <div class="container">
    <h3 align="center">Compras realizadas</h3>
    <br />
    <table class="table table-striped">
      <tr>
        <th>Fecha de Compra</th>
        <th>Nombre de la Orden</th>
        <th>Total de la Orden</th>
        <th> <a href="/home/index.php">Atras</a></th>
      </tr>
      <?php
        $result_array = $orden_model->consultaOrden($search); 
        
        if(!empty($result_array)){
        foreach ($result_array as $row) {
          echo "<tr>";
            echo "<td>" . $row['fecha_orden'] . "</td>";
            echo "<td>" . $row['nombre_orden'] . "</td>";
            echo "<td>" ."$". $row['total'] . "</td>";
            echo "<td>" .
                  "<a href='/consulta_orden/ver.php?id=" . $row['id'] . "'>Ver</a>".
                  "</td>";       
          echo "</tr>";
        }
        }else{
          echo "No hay ningún orden";
        }
      ?>
    </table>
  </div>

</body>
</html>
