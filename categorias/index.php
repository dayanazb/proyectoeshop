<?php
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';
?>
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <div class="container">
    <h3 aling="center">Categorias</h3>
    <br />
      <table  class="table table-striped">
         <tr>
            <th>Descripción</th>
            <th>Categoría Padre</th>
            <th><a href="/categorias/new.php">Nuevo</a></th>
        </tr>
        <?php
          include '../DbSetup.php';
          $result_array = $categoria_model->find();
          
          if(!empty($result_array)){
          foreach ($result_array as $row) {
            echo "<tr>";
              echo "<td>" . $row['descripcion'] . "</td>";
              echo "<td>" . $row['id_padre'] . "</td>";
              $id_categoria = $row['id'];
              $result = $producto_model->producto_categoria($id_categoria);
              echo "<td>" .
                   "<a href='/categorias/edit.php?id=" . $row['id'] . "'>Editar</a>". "   " .
                    "<a href='/categorias/delete.php?id=" . $row['id'] . "'>Eliminar</a>".
                    "</td>";
            echo "</tr>";
          }
        }else{
          echo "No existe esa categoria";
        }
        ?>
      </table>
</div>


</body>
</html>

