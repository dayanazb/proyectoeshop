<?php
  include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : '';
    $id_padre = isset($_POST['categoria']) ? $_POST['categoria'] : '';

    $categoria_model->update($id,$descripcion,$id_padre);
    return header("Location: /categorias");
  }
  $categoria = $categoria_model->find_for_id($id);
?>
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Editar Categoría</title>
</head>
<body>
  <div class="container">
    <h3>Editar Categoría</h3>
    <form method="POST">
      <table >
        <tr>
          <td>
            <label>Categoría:</label> 
          </td>
           <td><input type="text" name="descripcion" required autofocus value="<?php echo $categoria['descripcion']?>"></td>
        </tr>
        <tr>
          <td>
            <label>Categoría:</label>
          </td>
          <td>
            <?php 
            include '../DbSetup.php'; 
             $result_array = $categoria_model->find();
             echo '<select name="categoria">';
             echo '<option value="">Ninguno</option>';
            foreach ($result_array as $row) {
              echo '<option value="'.$row[id].'">'.$row[descripcion].'</option>';
            }
            echo '</select>';
            ?> 
        </tr>
        <tr><td><input type="submit" value="Salvar">
      <a href="/categorias">Atras</a></td></tr>
    </form>
 </div>

</body>
</html>
