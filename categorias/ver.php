<?php
include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
$search = isset($_GET['search']) ? $_GET['search'] : '';
?>
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <div class="container">
    <h3 align="center">Productos</h3>
    <form method="GET">
    </form>
    <table class="table table-striped">
      <tr>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Precio</th>
        <th>Stock</th>
        <th>Imagen</th>
        <th></th>
        </tr>
      <?php
        include '../DbSetup.php';
       $result_array = $producto_model->index1($search,$id);
       if(!empty($result_array)){
        foreach ($result_array as $row) {
          echo "<tr>";
            echo "<td>" . $row['nombre'] . "</td>";
            echo "<td>" . $row['descripcion'] . "</td>";
            echo "<td>" ."$". $row['precio'] . "</td>";
            echo "<td>" . $row['stock'] . "</td>";
            echo "<td>"  . "<img style=\"width: 22%;\" src='/imagenes/".$row['imagen'] . "'>" ."</td>";
             echo "<td>" .
                  "<a href='/carritos/new.php?id=" . $row['id'] . "'>Agregar al carrito de compra</a>".
                  "</td>";
          echo "</tr>";

        }
        }else{
          echo "No hay productos";
        }

      ?>
    </table>
</div>

</body>
</html>


