<?php
  include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
  $categoria = $categoria_model->find_for_id($id);
  $producto_categoria = $producto_model->producto_categoria($id);
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $categoria_model->delete($id);
    return header("Location: /categorias");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Eliminar Categorías</title>
</head>
<body>
  <div class="container">
    <h3 align="center">Eliminar Categoría</h3>
    <p>
      Descripción: <strong><?php echo $categoria['descripcion']; ?></strong>
    </p>
    <form method="POST">
     <?php
     if(empty($producto_categoria)){
      echo '<input type="submit" ' . 'value="Si">' ;
     echo '<a ' . 'href="/categorias">' . 'No' . ' </a>';
     }else{
      echo '<div class="alert info">' . 'Esta categoría tiene productos asociados, no se puede eliminar.'. '</div>';
      echo '<a ' . 'href="/categorias">' . 'Atras' . ' </a>';
     }
     ?>
      
      
    </form>
</div>
</body>
</html>

