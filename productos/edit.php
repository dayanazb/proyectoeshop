<?php
  include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $sku = $_POST['sku'];
    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $precio = $_POST['precio'];
    $stock = $_POST['stock'];
    $categoria =$_POST['categoria'];
    //Imagen
    $imagen=$_FILES['imagen']['name'];
    $tipo_imagen=$_FILES['imagen']['type'];
    $tamanno_imagen=$_FILES['imagen']['size'];

    if($tamanno_imagen<=3000000){
      if($tipo_imagen=="image/jpeg" || $tipo_imagen=="image/png" || $tipo_imagen=="image/jpg" || $tipo_imagen=="image/gif"){
        $ruta =$_SERVER['DOCUMENT_ROOT'].'/imagenes/';
        move_uploaded_file($_FILES['imagen']['tmp_name'], $ruta.$imagen);
      }
      else{
        echo "Solo se pueden subir imagenes";
      }     
    }else{
      echo "El tamaño es demasiado grande";
    }
    $skuUnico = $producto_model->confirmar($sku);
    if($skuUnico['sku'] === $sku || $skuUnico['sku'] != $sku ){
       $producto_model->update($id,$sku,$nombre,$descripcion,$imagen,$categoria,$stock,$precio);
      return header("Location: /productos");

    }else{
      echo "<h3>Este Sku ya fue utilzado</h3>";
    }

   }



  $producto = $producto_model->find($id);
?>
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Editar Producto</title>
</head>
<body>
  <div class="container">
    <h3 align="center">Editar Producto</h3>
    <form method="POST" enctype="multipart/form-data" >
      <table align="center" class="table">
        <tr>
          <td>
            <label>Sku:</label>
          </td>
          <td><input type="text" name="sku" required autofocus value="<?php echo $producto['sku']?>"></td>
        </tr>
        <tr>
          <td>
            <label>Nombre:</label>
          </td>
          <td><input type="text" name="nombre" required autofocus value="<?php echo $producto['nombre']?>"></td>
        </tr>
        <tr>
          <td>
            <label>Descripción:</label>
          </td>
          <td><input type="text" name="descripcion" required autofocus value="<?php echo $producto['descripcion']?>"></td>
        </tr>
        <tr>
          <td>
            <label for="imagen">Imagen:</label>
          </td>
          <td><input type="file" name="imagen" size="20"></td>
        </tr>
        <tr>
          <td>
            <label>Categoría:</label>
          </td>
          <td>
            <?php 
            include '../DbSetup.php'; 
             $result_array = $categoria_model->find();
             echo '<select name="categoria">';
            foreach ($result_array as $row) {
              echo '<option value="'.$row[id].'">'.$row[descripcion].'</option>';
            }
            echo '</select>';
            ?> 
        </tr>
        <tr>
          <td>
            <label>Stock:</label>
          </td>
          <td><input type="number" name="stock" required autofocus  value="<?php echo $producto['stock']?>"></td>
        </tr>
        <tr>
          <td>
            <label>Precio:</label>
          </td>
          <td><input type="number" name="precio" required autofocus value="<?php echo $producto['precio']?>"></td>
        </tr>
        

        
        </td>
        
        <tr><td align="center"><input type="submit" value="Guardar">
        <a href="/productos">Atras</a></td></tr>
    </form>
</div>

</body>
</html>
