<?php
 include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
  $producto = $producto_model->find($id);
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $producto_model->delete($id);
    return header("Location: /productos");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Eliminar Producto</title>
</head>
<body>
  <div class="container">
    <h3>Eliminar Producto</h3>
    <p>
      Esta seguro de eliminar el producto: <strong><?php echo $producto['nombre']; ?></strong>
    </p>
    <form method="POST">
      <input type="submit" value="Si">
      <a href="/productos">No</a>
    </form>
</div>
</body>
</html>
