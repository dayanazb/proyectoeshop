<?php
  //Include de verificar la sesión
  include '../seguridad/verificar_session.php';
  $search = isset($_GET['search']) ? $_GET['search'] : '';
  //Include para el manejo de BD
  include '../DbSetup.php';
  //Captura en una variable el id enviado del index 
  $id = $_GET['id'];
  //Captura en una variable el usuario enviado del index 
  $id_usuario= $_SESSION['usuario_id'];
     //Se valida si el request es "post", se trae de la base de datos los datos del carrito y se iguala las variables
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $cantidad_producto = $_POST['cantidad'];
      $producto = $producto_model->find($id);
      $precio_producto = $producto['precio'];
      $descripcion_producto = $producto ['descripcion'];
      //Se valida que el stock sea mayor a uno
    if ($producto['stock'] >= 1){
       //Se valida cantidad de productos sea menor o igual a los productos
    	 if($cantidad_producto <= $producto['stock']){ 
       //Se hace el insert a la BD de los datos del carrito de compra
      $carrito_model->insert($id_usuario,$cantidad_producto,$id,$precio_producto,$descripcion_producto);
       //Se resta del stock de productos
      $stock = ($producto['stock'] - $cantidad_producto);
       //Se actualiza en la base de datos
      $producto_model->updateStock($id,$stock);
       //Pasar a la pantalla principal de carrito de compra
			return header("Location: /carritos/index.php");
		   }else{
   			echo "<h1>Solo hay " . $producto['stock'] . " disponible. </h1>" ;
       }
    }else{
    	echo "<h1>No hay productos disponibles. </h1>" ;
    }
}
     //Se vuelve a llamar a la tabla de carrito compra 
   $prod = $producto_model->find($id);
  ?>
   <!-- Muestra la interfaz de crear el carrito de compra !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Agregar Producto</title>
</head>
<body>
  <div class="container">
    <h3>Agregar Producto al carrito</h3>
    <form method="POST">
      <label>Usuario:</label>
      <input type="text" value="<?php echo $_SESSION['nombre'] ?>" disabled>
      <br />
      <label>Producto:</label>
      <input type="text" value="<?php echo $_GET['id']?>" disabled>
      <br />
      <label>Disponible:</label>
      <input type="text" name="disponible" value="<?php echo $prod['stock']?>" disabled>
      <br />
      <label>Cantidad:</label>
      <input type="number" name="cantidad" required autofocus>
      <br />
      <input type="submit" value="Salvar">
      <a href="/carritos">Atras</a>
    </form>
</div>

</body>
</html>
