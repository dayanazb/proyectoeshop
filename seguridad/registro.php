<?php
  $titulo = 'Registro';
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include '../DbSetup.php';
    $primer_apellido = isset($_POST['primer_apellido']) ? $_POST['primer_apellido'] : '';
    $segundo_apellido = isset($_POST['segundo_apellido']) ? $_POST['segundo_apellido'] : '';
    $telefono = isset($_POST['telefono']) ? $_POST['telefono'] : '';
    $direccion = isset($_POST['direccion']) ? $_POST['direccion'] : '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $tipo_usuario = 'false';
    $password_confirmation = isset($_POST['password_confirmation']) ? $_POST['password_confirmation'] : '';
    if ($password != $password_confirmation) {
      echo "<h3>Las contraseñas no coinciden</h3>";
    } else {
      $usuario_model->insert($nombre, $primer_apellido, $segundo_apellido, $telefono, $email, $direccion, $password, $tipo_usuario);
      echo "<h3>Usuario registrado con éxito</h3>";
      return header("Location: /seguridad/login.php?id=" . $row['id']);
    }
  }

?>
<!DOCTYPE html>
<html>
<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../registro.css">
  <title>Registro</title>
</head>
<body>
<br />
<br />
<br />
  <div class="container" ">
  <div class="row">
    <div class="col-md-6 login-sec">
      <a href="/seguridad/login.php" class="btn btn-login float-right">Atras</a> 
        <h2 class="text-center">Registro</h2>
        <form method="POST" class="login-form">
  <div class="form-group">
      <label  class="text-uppercase">Nombre</label>
      <br />
      <input type="text" name="nombre" class="form-control">
      <br />
      <label for="exampleInputEmail1" class="text-uppercase">Primer Apellido</label>
      <br>
      <input type="text" name="primer_apellido"  class="form-control">
      <br>
      <label for="exampleInputEmail1" class="text-uppercase">Segundo Apellido</label>
      <br>
      <input type="text" name="segundo_apellido"  class="form-control">
      <br />
      <label for="exampleInputEmail1" class="text-uppercase">Telefono</label>
      <br/>
      <input type="text" name="telefono" class="form-control">
      <br />
      <label for="exampleInputEmail1" class="text-uppercase">Email</label>
      <br />
      <input type="email" name="email" class="form-control">
      <br>
      <label for="exampleInputEmail1" class="text-uppercase">Dirección</label>
      <br />
      <input type="text" name="direccion"  class="form-control">
      <br />
      <label for="exampleInputPassword1" class="text-uppercase">Contraseña</label>
      <br/>
      <input type="password" name="password" class="form-control" placeholder="">
      <br />
      <label for="exampleInputPassword1" class="text-uppercase">Confirmar Contraseña</label>
      <br />
      <input type="password" name="password_confirmation"  class="form-control" placeholder="">
      <br>
    <br>
  </div>

  <div class="form-check">
    <button type="submit" class="btn btn-login float-right">Registrarme</button>
  </div>
  
</form>
  </div>
  <div class="col-md-8 banner-sec"> 
  </div> 

  
    </div>
  </div>
</body>
</html>

