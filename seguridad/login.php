<?php
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include '../DbSetup.php';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $usuario = $usuario_model->find($email, $password);
    if (isset($usuario)) {
      session_start();
      $_SESSION['usuario_id'] = $usuario['id'];
      $_SESSION['nombre'] = $usuario['nombre'];
      $_SESSION['tipo_usuario'] = $usuario['tipo_usuario'];
      return header("Location: /home");
    } else {
      echo "<h3>Usuario o contraseña invalido</h3>";
    }
  }else{
  }
?>
<!DOCTYPE html>
<html>
<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../login.css">
  <title>Login</title>
</head>
<body>
<br />
<br />
<br />
<section class="login-block">
    <div class="container">
	<div class="row">
		<div class="col-md-4 login-sec">
		    <h2 class="text-center">Login E-Shop</h2>
		    <form method="POST" class="login-form">
  <div class="form-group">
      <label for="exampleInputEmail1" class="text-uppercase">Email</label>
      <br>
      <input type="email" name="email" class="form-control">
      <br>
      <label for="exampleInputPassword1" class="text-uppercase">Contraseña</label>
      <br>
      <input type="password" name="password" class="form-control" placeholder="">
    <br>
  </div>
  
  <div class="form-check">
    <button type="submit" class="btn btn-login float-right">Entrar</button>
    <br>
    <br>
    <a href="/seguridad/registro.php" class="btn btn-login float-right">Registrarse</a> 
  </div>
  
</form>

<div class="copy-text"><i class="fa fa-heart"></i> <a>Bienvenidos</a></div>
		</div>
		<div class="col-md-8 banner-sec">
    <div class="carousel-item active">
      
    </div>
            </div>	   
		    
		</div>
	</div>
</body>
</html>