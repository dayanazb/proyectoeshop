<?php
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';
?>
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <div class="container">
    <h3 aling="center">Estadísticas</h3>
   <br>
       <?php $t_usuario = strcmp($_SESSION['tipo_usuario'],"f"); ?>
       <?php if($t_usuario == 0) : ?>
	         <table  class="table table-striped">
	        	<tr>
	               <th>Total Productos Adquiridos</th>
	            </tr>
	       		<?php
	          include '../DbSetup.php';
	          $result_array = $orden_model->BuscarProductoCliente();
	          if(!empty($result_array)){
		          foreach ($result_array as $row) {
		            echo "<tr>";
		              echo "<td>" . $row. "</td>";
		            echo "</tr>";
		        	}
	        	}else{
		          echo "No existen productos";
		        }
	        	?>
	          <tr>
	            <th>Monto Total de Compras Realizadas</th>
	          </tr>
	       	<?php
	          include '../DbSetup.php';
	          $result_array = $orden_model->BuscarCompras();
	          if(!empty($result_array)){
	          foreach ($result_array as $row) {
	            echo "<tr>";
	              echo "<td>" . $row. "</td>";
	            echo "</tr>";
	        	}
	        	 }else{
		          echo "No existen compras realizadas";
		        }	
	        	?>
	          </table>
       <?php else : ?>
      <table  class="table table-striped">
        <tr>
          <th>Clientes Registrados</th>
        </tr>
        <?php
          include '../DbSetup.php';
          $result_array = $usuario_model->findChart();
          if(!empty($result_array)){
          foreach ($result_array as $row) {
            echo "<tr>";
              echo "<td>" . $row. "</td>";
            echo "</tr>";
        }
         }else{
          echo "No existen clientes registrados";
        }
        ?>
         <tr>
          <th>Productos Vendidos</th>
         </tr>
          <?php
          include '../DbSetup.php';
          $result_array = $orden_model->BuscarProductos();
          if(!empty($result_array)){
          foreach ($result_array as $row) {
            echo "<tr>";
            echo "<td>" . $row. "</td>";
            echo "</tr>";
        	}
        	 }else{
          echo "No existen productos";
         }
         ?>
         <tr>
          <th>Monto Total de Ventas</th>
        </tr>
          <?php
          include '../DbSetup.php';
          $result_array1 = $orden_model->BuscarVentas();
          if(!empty($result_array)){
          foreach ($result_array1 as $row) {
            echo "<tr>";
              echo "<td>" . $row. "</td>";
            echo "</tr>";
           }
            }else{
          echo "No existen ventas realizadas";
        }
        ?>
      </table>
      <?php endif; ?>
</div>
</body>
</html>

