<?php

namespace Models {

  class Carrito
  {
    private $connection;
    function __construct($connection)
    {
      $this->connection = $connection;
    }
    
    /**
     * Busca un carrito por medio del id
     * @param int $id 
     * @return carrito
     */
    public function find($id)
    {
      $result = $this->connection->executeSql("select * from carrito_compra where id = $id");
      return $this->connection->getResults($result)[0];
    }
    
    /**
     * Datos del carrito haciendo un join con el producto para mostrar datos del producto que estan relacionados a ese carrito
     * @param type $search 
     * @return carrito
     */
      public function index($search)
    {
        $sql = "SELECT carrito_compra.id,producto.nombre,precio_producto,descripcion_producto,cantidad_producto 
            FROM(carrito_compra INNER JOIN producto ON carrito_compra.id_producto = producto.id) WHERE carrito_compra.id_usuario = " . $_SESSION["usuario_id"];
      
      $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result);
    }


    /**
     * Crear un carrito de compra
     * @param int $id_usuario 
     * @param int $cantidad_producto 
     * @param int $id_producto 
     * @return void
     */
    public function insert($id_usuario,$cantidad_producto,$id_producto,$precio_producto,$descripcion_producto)
    {
      $sql = "INSERT INTO public.carrito_compra(id_usuario,cantidad_producto,id_producto,precio_producto,descripcion_producto) VALUES ('$id_usuario','$cantidad_producto','$id_producto','$precio_producto','$descripcion_producto')";
      $this->connection->executeSql($sql);
    }
    
     public function update($id, $descripcion,$id_usuario,$id_producto)
    {
      $sql = "UPDATE carrito_compra SET descripcion = '$descripcion' AND id_usuario = '$id_usuario' AND id_producto = '$id_producto'  WHERE id = $id";
      $this->connection->executeSql($sql);
    }

    /**
     * Eliminar un carrito de compra de un usuario
     * @param int $id_usuario 
     * @return void
     */
     public function delete_carrito($id_usuario)
    {
      $sql = "DELETE FROM public.carrito_compra WHERE id_usuario = $id_usuario";
      $this->connection->executeSql($sql);
    }

    /**
     * Elinar un producto que esta vinculado a un carrito de compra
     * @param int $id 
     * @return void
     */
    public function deleteProducto($id)
    {
      $sql = "DELETE  FROM public.carrito_compra WHERE id = $id";
      $this->connection->executeSql($sql);
    }
  }
}
