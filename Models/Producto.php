<?php

namespace Models {

  class Producto
  {
    private $connection;
    function __construct($connection)
    {
      $this->connection = $connection;
    }

    /**
     * Busca un producto por medio del id
     * @param int $id 
     * @return arreglo del producto
     */
    public function find($id)
    {
      $result = $this->connection->executeSql("select * from public.producto where id = $id");
      return $this->connection->getResults($result)[0];
    }

     /**
      *Busca si ya hay un sku registrado igual al del parametro.
      * @param string $sku 
      * @return sku
      */
     public function confirmar($sku)
    {
      $result = $this->connection->executeSql("SELECT * FROM public.producto WHERE sku = '$sku'");
      return $this->connection->getResults($result)[0];
    }

 /**
     * Busca todos los productos relacionados a un a una categoria
     * @param type $search 
     * @param int $id 
     * @return productos
     */
    public function producto_categoria($id)
    {
      $sql = "select * from public.producto where id_categoria = $id";

      $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result);
    }
   
    /*
    public function categoria($id_categoria)
    {
      $result = $this->connection->executeSql("SELECT * FROM public.categoria WHERE id = '$id_categoria'");
      return $this->connection->getResults($result)[0];
    }
    */

    /**
     * Buscar todos los productos registrados
     * @return productos
     */
    public function findProductos()
    {
      $result = $this->connection->executeSql("select * from public.producto");
      return $this->connection->getResults($result);
    }

    public function findProductosStock()
    {
      $result = $this->connection->executeSql("select * from public.producto");
      return $this->connection->getResults($result);
    }
    
    /**
     * Busca todos los productos relacionados a un usuario para mostrarlos en el carrito
     * @param type $search 
     * @param int $id 
     * @return productos
     */
    public function index1($search, $id)
    {
      $sql = "select * from public.producto where id_categoria = $id";
      if ($search) {
        $search_criteria = [];
        array_push($search_criteria, "id = " . intval($search));
        array_push($search_criteria, "sku ilike '%" . $search ."%'");
        array_push($search_criteria, "nombre ilike '%" . $search ."%'");
        array_push($search_criteria, "descripcion ilike '%" . $search ."%'");
        array_push($search_criteria, "imagen ilike '%" . $search ."%'");
        array_push($search_criteria, "stock = " . doubleval($search));
        array_push($search_criteria, "precio = " . numericval($search));
        
        $sql .= " where " . join($search_criteria, ' or ');
      }
      $sql .= " order by id";
      $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result);
    }

   
  
    /**
     * Metodo que crea un nuevo producto
     * @param string $sku 
     * @param string $nombre 
     * @param string $descripcion 
     * @param string $imagen 
     * @param int $categoria 
     * @param int $stock 
     * @param int $precio 
     * @return void
     */
    public function insert($sku, $nombre, $descripcion, $imagen, $categoria, $stock, $precio)
    {
      $sql = "INSERT INTO public.producto(sku,nombre,descripcion,imagen,id_categoria,stock,precio) VALUES ('$sku', '$nombre', '$descripcion', '$imagen', '$categoria', '$stock', '$precio')";
      $this->connection->executeSql($sql);
      
    }
    
    /**
     * Edita un producto
     * @param int $id 
     * @param string $sku 
     * @param string $nombre 
     * @param string $descripcion 
     * @param string $imagen 
     * @param int $categoria 
     * @param int $stock 
     * @param int $precio 
     * @return void
     */
    public function update($id, $sku, $nombre, $descripcion, $imagen, $categoria, $stock, $precio)
    {
      $sql = "UPDATE public.producto SET sku = '$sku', nombre = '$nombre',  descripcion = '$descripcion', imagen = '$imagen', id_categoria = '$categoria', stock = '$stock', precio = '$precio'  WHERE id = $id";
      $this->connection->executeSql($sql);
    }


    /**
     * Modificar el stock cuando se agrega un producto al carrito y cuando se elimina del carrito
     * @param int $id 
     * @param int $stock 
     * @return void
     */
    public function updateStock($id,$stock)
    {
      $sql = "UPDATE public.producto SET  stock = '$stock' WHERE id = $id";
      $this->connection->executeSql($sql);
    }
     

    /**
     * Elimina un producto de la base de datos
     * @param int $id 
     * @return void
     */
    public function delete($id)
    {
      $sql = "DELETE FROM public.producto WHERE id = $id";
      $this->connection->executeSql($sql);
    }
  }
}
