<?php

namespace Models {

  class Categoria
  {
    private $connection;
    function __construct($connection)
    {
      $this->connection = $connection;
    }
    
    /**
     * Buca una categoria por medio de un id
     * @param int $id 
     * @return categoria
     */
    public function find_for_id($id)
    {
      $result = $this->connection->executeSql("select * from public.categoria where id = $id");
      return $this->connection->getResults($result)[0];
    }
    
    /**
     * Busca todas las categorias.
     * @return categorias
     */
    public function find()
    {
      $result = $this->connection->executeSql("select * from public.categoria");
      return $this->connection->getResults($result);
    }
  
  
    /**
     * Crear una nueva categoria
     * @param string $descripcion 
     * @return void
     */
    public function insert($descripcion,$id_padre)
    {
      $sql = "INSERT INTO public.categoria(descripcion,id_padre) VALUES ('$descripcion','$id_padre')";
      $this->connection->executeSql($sql);
    }
    
    /**
     * Editar una categoria
     * @param int $id 
     * @param string $descripcion 
     * @return void
     */
    public function update($id,$descripcion,$id_padre)
    {
      $sql = "UPDATE public.categoria SET descripcion = '$descripcion', id_padre = '$id_padre' WHERE id = $id";
      $this->connection->executeSql($sql);
    }
    
    /**
     * Eliminar una categoria
     * @param int $id 
     * @return void
     */
    public function delete($id)
    {
      $sql = "DELETE FROM public.categoria WHERE id = $id";
      $this->connection->executeSql($sql);
    }
  }
}
