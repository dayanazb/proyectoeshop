<?php

namespace Models {

  class Usuario {

    private $connection;
    function __construct($connection)
    {
      $this->connection = $connection;
    }

    /**
     * Metodo de que busca la cotraseña y el email para verificar si el usuario esta registrado.
     * @param email  $email  
     * @param password  $password 
     * @return usuario
     */
    public function find($email, $password)
    {
      $result = $this->connection->executeSql("SELECT * FROM usuario WHERE email = '$email' and password = md5('$password')");
      return $this->connection->getResults($result)[0];
    }

    /**
     * 
     */
   	public function findChart()
    {
      $result = $this->connection->executeSql("SELECT COUNT(id) FROM usuario");
        return $this->connection->getResults($result)[0];
    }
    
    /**
     * Metodo para insertar un nuevo usuario en la base de datos.
     * @param string $nombre 
     * @param string $primer_apellido 
     * @param string $segundo_apellido 
     * @param int  $telefono 
     * @param string $email 
     * @param string $direccion 
     * @param password $password 
     * @param boolean $tipo_usuario 
     * @return void
     */
    public function insert($nombre, $primer_apellido, $segundo_apellido, $telefono, $email, $direccion, $password, $tipo_usuario)
    {
      $sql = "INSERT INTO public.usuario(nombre, primer_apellido, segundo_apellido, telefono, email, direccion, password, tipo_usuario) VALUES ('$nombre', '$primer_apellido', '$segundo_apellido', '$telefono', '$email', '$direccion', md5('$password'), $tipo_usuario)";
      $this->connection->executeSql($sql);
    }
  }
}
